kali2-live-build
=======

## Overview

This project will host various live-build configurations of Kali Linux.
You can get the original live-build-config provided by Kali Linux from their Git:
```
git clone git://git.kali.org/live-build-config.git
```

- - -

## Important

Before running the build script make sure you have the proper things installed:
```
apt-get install live-build devscripts debootstrap
```

Make sure you use a Kali Linux as your base development platform.
- - -
