Custom Kali build with XFCE and different look & tweaks
=======

## Screenshot
![Alt text](xfce_kali_phat.png?raw=true "Optional Title")
- - -

## Instructions

To build simply run the build.sh script from the "live-build-config" directory as follows:
```
./build.sh --variant xfce --verbose
```
- - -

## Contains

1. Modified theme
2. XFCE panel and launcher configurations under /root/.config/
3. Thunar configuration
4. Gtk-Bookmarks
5. Keyboard shortcuts
6. Removed user directories like {Downloads,Music, etc}
7. Changed terminal font size to something smaller
8. Fixed theme to work on taskbar as well
9. Added login wallpaper
10. Moved themes to /usr/share/themes/
11. Fixed lock-screen
12. Made a copy of all XFCE configuration from /etc/xdg/ in /root/.config/xfce4/ (not mandatory but good if later on we decide on more users)
13. Added hooks that will hide the e-mail app in Kali Menu and fix VLC running as root
14. Configured lightdm to show clock and use the same font
15. Fixed Lua error from Wiresark
- - -

## Additional information

If you want to use multiarch:
```
root@kali:~# dpkg --add-architecture i386
root@kali:~# apt-get update
root@kali:~# apt-get install wine32
```
- - -
